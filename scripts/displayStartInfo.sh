#!/bin/bash

echo -e "\e[1;92mWelcome to cordova npm version info sync\e[0m";
echo -e "\e[1;36mIn order for this plugin to work as intended, please follow these instructions:\e[0m";
echo -e "\e[1;36mPlease, create 'config.xml.template' file, with 'APP_VERSION' and 'VERSION_CODE' parameters.\e[0m";
echo -e "\e[1;36mExample: <widget id=\"com.example.app\" version=\"APP_VERSION\" android-versionCode=\"VERSION_CODE\"...> .\e[0m";
echo -e "\e[1;36mNote: if you're using a git repository, it's recommended to place 'config.xml' file on .gitignore, as this file will be generated automatically from the provided template.\e[0m";
