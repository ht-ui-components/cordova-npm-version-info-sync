#!/bin/bash

# Get app version
app_version=$(cat $(pwd)/package.json | jq -r ".version");
echo -e "\e[1;92mGot app version: $app_version\e[0m";
# Get Android version code from app version
IFS='.' read -ra app_version_data <<< "$app_version";
app_version_code="";
for i in "${app_version_data[@]}"
do
    app_version_code=$app_version_code$i;
    if [ $i -lt 10 ] ; then
        app_version_code="${app_version_code}0";
    fi
done
app_version_code="${app_version_code}0";
echo -e "\e[1;36mUsing Android version code: $app_version_code\e[0m";
cp config.xml.template config.xml > /dev/null;
if [ $? -ne 0 ] ; then
    echo -e "\e[1;31mConfig template not found. Could not sync cordova version info.\e[0m";
    echo -e "\e[1;31mPlease, create 'config.xml.template' file, with 'APP_VERSION' and 'VERSION_CODE' parameters.\e[0m";
    echo -e "\e[1;31mExample: <widget id=\"com.example.app\" version=\"APP_VERSION\" android-versionCode=\"VERSION_CODE\"...> .\e[0m";
    echo -e "\e[1;31mNote: if you're using a git repository, it's recommended to place 'config.xml' file on .gitignore, as this file will be generated automatically from the provided template.\e[0m";
    exit -1;
fi
sed -i -e "s/APP_VERSION/$app_version/g" config.xml;
sed -i -e "s/VERSION_CODE/$app_version_code/g" config.xml;

