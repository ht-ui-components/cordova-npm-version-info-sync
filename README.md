# cordova-npm-version-info-sync

This Cordova plugin provides an automatic synchronization between npm package.json version and Cordova app version specified on config.xml file. It also generates an Android version code which is dependent on said version number.

In order for this plugin to work as intended, please follow these instructions:

Please, create 'config.xml.template' file, with 'APP_VERSION' and 'VERSION_CODE' parameters.
Example:
  <widget id="com.example.app" version="APP_VERSION" android-versionCode="VERSION_CODE"...> .
  
Note: if you're using a git repository, it's recommended to place 'config.xml' file on .gitignore, as this file will be generated automatically from the provided template.
